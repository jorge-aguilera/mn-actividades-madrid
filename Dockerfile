FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
COPY build/libs/mn-actividades-madrid-*-all.jar mn-actividades-madrid.jar
EXPOSE 8080
ARG MN_ARGS
ENV JAVA_OPTS=$MN_ARGS
CMD java -v -XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar mn-actividades-madrid.jar
