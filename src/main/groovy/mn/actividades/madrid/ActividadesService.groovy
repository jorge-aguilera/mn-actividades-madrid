package mn.actividades.madrid

import groovy.time.Duration
import groovy.transform.CompileStatic
import groovy.transform.Synchronized
import groovy.transform.TypeCheckingMode
import mn.telegram.Location

import javax.annotation.PostConstruct
import javax.inject.Singleton
import java.text.SimpleDateFormat

@Singleton
class ActividadesService {

    List<Actividad> actividades = []

    Date lastUpdated

    ActividadesService(){
    }

    Actividad findActividad( int id){
        actividades.find{ it.id == id}
    }

    private void download() {
        File root = new File('build')
        root.mkdirs()
        File csv = new File(root, 'eventos.csv')
        if( csv.exists() )
            return
        csv.withWriter('UTF-8') { w ->
            String url = 'https://datos.madrid.es/egob/catalogo/206974-0-agenda-eventos-culturales-100.csv'
            w << new InputStreamReader(new URL(url).openStream(), 'iso-8859-1')
        }
    }

    @Synchronized
    void init() {

        if( lastUpdated ){
            Calendar c = Calendar.instance
            c.add Calendar.HOUR_OF_DAY, -12
            if( lastUpdated.after(c.time) ){
                actividades = []
            }
        }

        if( actividades.size() )
            return

        download()

        File csv = new File(new File('build'), 'eventos.csv')
        csv.eachLine('UTF-8'){ line ->
            def match = line =~ /"([^"]*)"/
            if( !match )
                return

            List<String> fields = match.collect{ List items ->
                items[1].toString().replaceAll(';',' ').trim()
            } as List<String>

            try{
                Date start = new SimpleDateFormat('yyyy-MM-dd').parse(fields[6].split(' ').first())
                Date end = new SimpleDateFormat('yyyy-MM-dd').parse(fields[7].split(' ').first())

                Actividad actividad = new Actividad(
                        id: actividades.size(),
                        title: fields[0],
                        description: fields[9],
                        latitude: fields[17] as float,
                        longitude: fields[18] as float,
                        start: start,
                        end: end,
                        hour: fields[8],
                        url : fields[10]
                )
                actividades.add actividad
            }catch( Exception e ){
                println e.message
            }
        }
    }

    @CompileStatic(TypeCheckingMode.SKIP)
    List<Actividad> buscaActividades ( int when, Location location){
        List<Actividad> ret
        use (groovy.time.TimeCategory) {
            Calendar today = Calendar.instance
            Calendar tomorrow = Calendar.instance
            Calendar oneWeek = Calendar.instance

            tomorrow.add(Calendar.DAY_OF_YEAR,1)
            oneWeek.add(Calendar.DAY_OF_YEAR,7)

            ret = actividades.findAll {
                Calendar c = Calendar.instance
                c.time = it.start
                switch (when) {
                    case 0:
                        return c.get(Calendar.DAY_OF_YEAR) == today.get(Calendar.DAY_OF_YEAR)
                    case 1:
                        return c.get(Calendar.DAY_OF_YEAR) == tomorrow.get(Calendar.DAY_OF_YEAR)
                    case 7:
                        return c.get(Calendar.DAY_OF_YEAR) >= today.get(Calendar.DAY_OF_YEAR) &&
                            (c.get(Calendar.DAY_OF_YEAR) - today.get(Calendar.DAY_OF_YEAR)) < 7
                    default:
                        return null
                }
            }
            ret.sort { Actividad a ->
                location.metersTo(a.latitude, a.longitude)
            }
        }
        ret
    }

}
