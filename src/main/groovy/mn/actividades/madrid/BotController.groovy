package mn.actividades.madrid

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post
import io.reactivex.Single
import mn.telegram.InlineKeyboardButton
import mn.telegram.InlineKeyboardMarkup
import mn.telegram.KeyboardButton
import mn.telegram.Location
import mn.telegram.Message
import mn.telegram.ReplyKeyboardMarkup
import mn.telegram.ReplyMarkup
import mn.telegram.TelegramClient
import mn.telegram.Update

@Controller('/${telegram.token}/bot')
class BotController {

    ActividadesService actividadesService

    SessionManager sessionManager

    TelegramClient telegramClient

    BotController (ActividadesService actividadesService,  TelegramClient telegramClient, SessionManager sessionManager ){
        this.actividadesService = actividadesService
        this.telegramClient = telegramClient
        this.sessionManager = sessionManager
    }

    void sayHello(String chatId){

        Message msg = new Message(chat_id: chatId, text: """
Soy un Bot que puede indicarte las actividades programadas
en Madrid cercanas a tí. Para ello tienes que mandarme tu posicion

Selecciona `enviar adjunto` (el clip) y pincha en Ubicación.
Una vez me mandes donde estas te enviaré una lista de actividades
que se van a realizar cerca de ella hoy mismo, mañana o en una semana


Made with 💙 by Puravida Software 
Usando los catálogos de datos abiertos del 
[Portal del Ayuntamiento de Madrid](https://www.madrid.es/portales/munimadrid/es/Inicio/Actualidad/Actividades-y-eventos/?vgnextfmt=default&vgnextchannel=ca9671ee4a9eb410VgnVCM100000171f5a0aRCRD&vgnextoid=ca9671ee4a9eb410VgnVCM100000171f5a0aRCRD) 
""")
        telegramClient.sendMessage(msg).subscribe()
    }

    @Post('/')
    String index(Update update){
        println update

        Single.create({ emitter ->

            actividadesService.init()

            String chatId = update.message ? update.message.chat.id : update.callback_query.message.chat.id
            Session session = sessionManager.findSession(chatId) ?: new Session(chatId: chatId)
            String cmd

            if( update.message ) {
                if (update.message.location ) {
                    session.location = update.message.location
                }else{
                    cmd = update.message.text
                }
            }

            if( update.callback_query ){
                cmd = update.callback_query.data
            }

            if( ['0','1','7'].contains(cmd)){
                session.when = cmd as int
            }

            sessionManager.save(session)

            if( session.location != null && session.when == -1){
                askWhen(session)
            }else {
                if (session.location == null && session.when != -1) {
                    askLocation(session)
                }else{
                    if (['/start', '/info', '/help'].contains(cmd)) {
                        sayHello(session.chatId)
                    }else {
                        if (session.location != null && session.when != -1) {
                            sendActividades(session)
                        }
                    }
                }
            }
            emitter.onSuccess("done")

        }).subscribe()

        "done"
    }

    void sendActividades(Session session) {
        int size = session.when < 7 ? 4 : 10
        List<Actividad> ret = actividadesService.buscaActividades(session.when, session.location).reverse()
        String txt = ret.take(size).sort{ it.start }.collect{ it.asMarkdown }.join('\n')
        txt += """

(Mostrando las $size actividades más cercanas a esta ubicación de ${ret.size()} existentes)
"""
        Message msg = new Message(
                chat_id: session.chatId,
                text: txt
        )
        telegramClient.sendMessage(msg).subscribe({}, {})
    }

    void askWhen(Session session){
        telegramClient.sendMessage(buildMessageWithKeyboard(session.chatId, session.location)).subscribe({},{})
    }

    void askLocation(Session session){
        telegramClient.sendMessage(buildMessage(session.chatId)).subscribe({},{})
    }

    Message buildMessage(String chatId){
        new Message(
                chat_id: chatId,
                text:"Utiliza el botón de adjuntar para enviarme tu localización"
        )
    }

    Message buildMessageWithKeyboard(String chatId, Location location){
        new Message(
                chat_id: chatId,
                text:"Voy a buscar actividades en #Madrid cerca de tí. Elige para cuando las quieres",
                reply_markup: buildKeyboard(chatId, location)
        )
    }

    ReplyMarkup buildKeyboard(String chatId, Location location){
        List<InlineKeyboardButton> buttons = []

        buttons.add new InlineKeyboardButton(
                text: "Hoy",
                callback_data: "0"
        )
        buttons.add new InlineKeyboardButton(
                text: "Mañana",
                callback_data: "1"
        )
        buttons.add new InlineKeyboardButton(
                text: "Toda la semana",
                callback_data: "7"
        )

        List<List<InlineKeyboardButton>> keyboard = buttons.collate(1)
        new InlineKeyboardMarkup(inline_keyboard: keyboard)
    }

}
