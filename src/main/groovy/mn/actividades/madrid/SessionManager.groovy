package mn.actividades.madrid

interface SessionManager {

    Session findSession( String chatId )

    Session save(Session session)

}
