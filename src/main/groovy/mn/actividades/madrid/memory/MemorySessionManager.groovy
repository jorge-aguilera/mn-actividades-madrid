package mn.actividades.madrid.memory

import mn.actividades.madrid.Session
import mn.actividades.madrid.SessionManager

import javax.inject.Singleton

@Singleton
class MemorySessionManager implements SessionManager{

    Map<String,Session> sessions = [:]

    Session findSession( String chatId ){
        sessions[chatId]
    }

    @Override
    Session save(Session session) {
        sessions[session.chatId] = session
        session
    }
}
