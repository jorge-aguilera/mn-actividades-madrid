package mn.actividades.madrid

import java.text.SimpleDateFormat

class Actividad {
    int id
    String title
    String description
    float latitude
    float longitude
    Date start
    Date end
    String hour
    String url

    String getAsMenuItem(){
        title.take(40)
    }

    String getAsMarkdown(){
        String d = new SimpleDateFormat('yyyy/MM/dd').format(start)
"""
🎯 *$title* ($d $hour)
${description.take(200)}

[Más info]($url)
"""
    }
}
