package mn.telegram

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.http.annotation.QueryValue
import io.micronaut.http.client.annotation.Client
import io.micronaut.http.client.multipart.MultipartBody
import io.reactivex.Single

@Client('https://api.telegram.org')
interface TelegramClient {

    @Post('/bot${telegram.token}/sendMessage')
    Single<Message> sendMessage(@Body Message message)

    @Post(value='/bot${telegram.token}/sendPhoto', produces = MediaType.MULTIPART_FORM_DATA)
    Single<Message> sendPhoto(@Body MultipartBody photo)

    @Post(value='/bot${telegram.token}/sendAnimation', produces = MediaType.MULTIPART_FORM_DATA)
    Single<Message> sendAnimation(@Body MultipartBody animation)

    @Get(value='/bot${telegram.token}/sendLocation')
    Single<Message> sendLocation(@QueryValue("chat_id") String chat_id,
                                 @QueryValue("latitude") float latitude,
                                 @QueryValue("longitude") float longitude)

    @Post(value='/bot${telegram.token}/sendAudio', produces = MediaType.MULTIPART_FORM_DATA)
    Single<Message> sendAudio(@Body MultipartBody audio)

    @Post(value='/bot${telegram.token}/sendVoice', produces = MediaType.MULTIPART_FORM_DATA)
    Single<Message> sendVoice(@Body MultipartBody voice)

    @Post(value='/bot${telegram.token}/answerCallbackQuery')
    Single<Message>answerCallbackQuery(String callback_query_id, String text)

    @Post(value='/bot${telegram.token}/answerCallbackQuery')
    Single<Message>answerCallbackQuery(String callback_query_id, String text, boolean show_alert)

    @Post(value='/bot${telegram.token}/answerCallbackQuery')
    Single<Message>answerCallbackQuery(String callback_query_id, String text, boolean show_alert, int cache_time)
}
