package mn.actividades.madrid

import io.micronaut.test.annotation.MicronautTest
import spock.lang.Specification

import javax.inject.Inject

@MicronautTest
class ActividadesServiceSpec extends Specification{

    @Inject
    ActividadesService actividadesService

    void "init service"(){
        setup: "clean a service"
        File csv = new File(new File('build'), 'eventos.csv')
        if( csv.exists() )
            csv.delete()

        when: "init the service"
        actividadesService.init()

        then: "a csv is downloaded"
        new File(new File('build'), 'eventos.csv').exists()

        and: "the service has actividades"
        actividadesService.actividades.size()

    }

}
